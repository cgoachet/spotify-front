# Prerequis
Avoir node 8+ et angular 6.0.3+

# Executer le projet
Pour lancer le projet front :
* se placer dans le r�pertoir que l'on veut importer (ex : cd ~/Documents/coding-challenge)
* git clone https://cgoachet@bitbucket.org/cgoachet/spotify-front.git
* ng serve

# Utilisation
Par d�faut le projet se lance sur http://localhost:4200.
L'utilisateur peut rechercher un album en rentrant le nom de l'album dans le champ title puis en cliquant sur 'Search !'.
Du au manque de temps, le back n'est pas connect� � l'api Spotify.