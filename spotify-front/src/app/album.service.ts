import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AlbumModel } from './model/album.model';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient){}

  public searchAlbum(title:string) {
    return this.http.get<Array<AlbumModel>>(`${environment.spotifyApiUrl}?title=${title}`);
  }
}
